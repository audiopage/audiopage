// If the site is being built in a webcontainer
const isWebContainer = process.env.AUDIOPAGE_ENV === "webcontainer";

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: !isWebContainer }
})
