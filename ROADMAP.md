## V1 Features

- Artist profile, album, and track pages
- Site-wide playback controls (Nuxt in SPA mode makes this easy-ish)
- Lyrics & closed captions: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/audio#accessibility_concerns
- Media Session API: https://developer.mozilla.org/en-US/docs/Web/API/Media_Session_API
- Purchase options:
  - Streaming only
  - Free download
  - Soft paywall (Stripe / PayPal / Ko-fi embed)
  - Hard paywalls are not achievable as a static site!
    But we could enable this by allowing the user to present a purchase page *without* download options
    (i.e. the artist uses another service to provide the files after purchase, like a private dropbox folder)
- CMS/Generator
  - Runs entirely in-browser using [WebContainers](https://webcontainers.io)
  - Abstraction of "deployment channels" for exporting as static/dynamic (default) or deploying to a hosting provider
  - Selecting/uploading track files must be persistent + saved in IndexedDB
    - The [File System API](https://developer.mozilla.org/en-US/docs/Web/API/File_System_API) allows its `FileSystemHandle` objects to be serialized in IndexedDB for later access
    - Users will need to be prompted for access if the file is moved/changed
  - Use the [WebCodecs API](https://developer.mozilla.org/en-US/docs/Web/API/WebCodecs_API) when available (it isn't in Firefox!) - otherwise, may need to use [ffmpeg in WASM](https://ffmpegwasm.netlify.app/docs/overview/)
  - Memory/disk limits might be a concern; ideally, everything should be streamed directly from file to disk.
    * Considering that individual audio files can be GIGABYTES large, and an artist's entire portfolio could be many times larger than the available memory.
    * In the case of ffmpeg/wasm, individual audio files may need to be written to a temporary location. These should be handled one at a time to avoid hitting any limits.

**Routes:**

- `/` -> Latest releases (all artists on the site), or the artist profile if it's a single-artist site
- `/artist/{id}` -> Artist profile & releases
- `/album/{id}`
  * `/get`
  * `/embed`
- `/track/{id}`
  * `/get`
  * `/embed`

## V2 and onwards... (excessively ambitious goals)

- Live CMS login/editing on dynamic sites
- ActivityPub
  * Support for track/album "reviews"
  * Allow following, and send info about new releases
- Purchase protection:
  * Encrypted streaming, or enforced stream limits (the bandcamp "You can only stream this 10 times before purchasing)
  * Hard paywalls with Stripe integration
  * Stored "proof of purchase", email, and possibly a "Sign in with Mastodon" option?
- Blog posts / other content adjacent to music
  * Newsletters? e.g. [writefreely.org](https://developers.write.as/docs/api/#introduction) or [ghost.org](https://ghost.org/docs/introduction/)
- Ideas to address user concerns; creating/sharing playlists...
  * What about a [bookwyrm.social](https://bookwyrm.social) for music? i.e. the listener-facing interface is a completely separate app
    * This sounds very similar to [Freq](https://publicinfrastructure.org/2023/05/24/introducing-freq/)
  * Look into [MusicRecording](https://schema.org/MusicRecording) json-ld data, or providing extended info in RSS
- Better self-hosting / community hosting experience
  * Package for [YunoHost](https://yunohost.org/#/) and [coopcloud.tech](https://docs.coopcloud.tech/operators/tutorial/)
  * Would single-sign-on (SSO) configuration be useful?
